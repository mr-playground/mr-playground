(ns mr-playground.sin-sun
  (:use quil.core))

(def -canvas (atom {:width 500
             :height 500}))

(defn get-height [canvas]
  (:height @canvas))

(defn get-width [canvas]
  (:width @canvas))

(defn find-center [canvas]
  [(/ (get-width canvas) 2) (/ (get-height canvas) 2)])

(defn quil-demo-setup []
  (frame-rate 250)
  (background 200))

(defn pnoise [d func]
  (+ (random 100) (func d)))

(defn nnoise [d func]
  (- (func d) (random 100)))

(defn draw-point-in-the-mid []
  (stroke (red 100))
  (fill (red 100))
  (let [d (find-center -canvas)]
    (stroke-weight 10)
    (point (first d) (second d))
    (stroke-weight 1)
    (point (pnoise d first) (-> d second))
    (point (-> d first) (pnoise d second))
    (point (nnoise d first) (-> d second))
    (point (-> d first) (nnoise d second))
  ))

(defn quil-demo []
  (defsketch example                  ;; Define a new sketch named example
    :setup quil-demo-setup                      ;; Specify the setup fn
    :draw draw-point-in-the-mid
    :size [(get-width -canvas) (get-height -canvas)])
  )
