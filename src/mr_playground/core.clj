(ns mr-playground.core
  (:use quil.core))

(defn -main [& args]
  (println args))

(defn quil-demo-setup []
  (smooth)
  (frame-rate 250)
  (background 200))

(defn random-color []
  (color (random  255) (random  255) (random  255)  ))

(defn quil-demo-draw []
  (stroke (random-color))             ;; Set the stroke colour to a random grey
  (stroke-weight (random 50))       ;; Set the stroke thickness randomly
  (fill (random-color))               ;; Set the fill colour to a random grey
  (let [diam (random 100)           ;; Set the diameter to a value between 0 and 100
        x    (random (width))      ;; Set the x coord randomly within the sketch
        y    (random (height))]     ;; Set the y coord randomly within the sketch
    (point (* (width) (noise (random (width)) (random (width))))
           (* (height) (noise (random (height) (random (height)))) (noise (random (height) (random (height))))
              ))))

(def x (atom 0))
(def y (atom 0))

(def frame (atom 0))

(defn move-x [x]
  (if (<= (width) @x)
    (reset! x 0)
    (swap! x inc)))

(defn random-range [width]
  (random (+ 150 (/ 2 width))  (+ 5 (/ width 2))))


(defn deg-cycle [frame]
  (if (>= @frame 360)
    (reset! frame 0)
    (swap! frame inc)))

(defn demo2 []
  (stroke (red 10))
  (stroke-weight 5)
  (fill (red 10))
  (let [start-x (move-x x)
        start-y (+ (/ (height) 2) (* 50 (sin (radians (deg-cycle frame)))))]
    (point start-x start-y)))


(defn quil-demo [ ]
(defsketch example                  ;; Define a new sketch named example
  :title "Oh so many grey circles"  ;; Set the title of the sketch
  :setup quil-demo-setup                      ;; Specify the setup fn
  :draw demo2                        ;; Specify the draw fn
  :size [800 300])                  ;; You struggle to beat the golden ratio
)
