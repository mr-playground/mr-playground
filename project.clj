(defproject mr-playground "1.0.0-SNAPSHOT"
  :description "my clojure playground on clojars"
  :plugins [[lein-clojars "0.9.1"]]
  :main mr-playground.core
  :dependencies [[org.clojure/clojure "1.5.1"]
		 [quil "1.7.0"]])
